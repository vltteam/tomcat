FROM tomcat

MAINTAINER vltgroup, admin@vltgroup.com

# Install ttf-dejavu which is required by JavaMelody
RUN apk add --update ttf-dejavu && rm -rf /var/cache/apk/*

# Clean up webapps directory
RUN rm -rf /usr/local/tomcat/webapps/*
